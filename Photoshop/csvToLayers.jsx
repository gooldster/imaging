/**
 * A Photoshop script for adding data from a comma-separated file into groups.
 * This script works for a specfic situation, but can be modified to work for any similar needs.
 * 
 * For this project specifically:
 *  - The .psd file must be present in the same folder as the .csv
 *  - The yellow-brushstroke.png must be in the .psd file AND must be labeled "yellow-brushstroke".
 *
 * @link   https://bitbucket.org/gooldster/imaging/src/master/
 * @author Aaron Goold.
 */

 /**
  * String.prototype.includes() polyfill
  */
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
      'use strict';
      if (typeof start !== 'number') {
        start = 0;
      }
  
      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }

try {
    var activeDoc = app.activeDocument;
    var csv = new File(activeDoc.path + '/markerData.csv');
    csv.open('r');

    var getInput = prompt('Enter: ', 'something');

    while(!csv.eof) {
        var currentLine = csv.readln();

        if(currentLine.includes(getInput)) {
            continue;
        }

        var textLayer = activeDoc.artLayers.add(); // This is the ENTIRE layer.
        textLayer.kind = LayerKind.TEXT;
        textLayer.name = currentLine;

        var textItem = activeDoc.artLayers[textLayer.name].textItem;
        textItem.contents = currentLine;
        textItem.capitaliztion = TextCase.ALLCAPS;
        textItem.font = 'FuturaStd-ExtraBold';
        textItem.justification = Justification.CENTER;
        textItem.tracking = 0;

        var xPosition = new UnitValue('2.75 inches');
        var yPosition = new UnitValue('2.10 inches');
        textItem.position = [xPosition, yPosition];

        var fontSize = new UnitValue('35 pixels');
        textItem.size = fontSize;

        textItem.useAutoLeading = false;
        var leadingSize = new UnitValue('34 points');
        textItem.leading = leadingSize;

        // Create groups.
        var group = activeDoc.layerSets.add();
        group.name = currentLine;

        // Copy yellow-brushstroke, move yellow-brushtroke and text into group.
        var yellowBrushStroke = activeDoc.layers['yellow-brushstroke'];
        yellowBrushStroke.duplicate(group, ElementPlacement.INSIDE);

        textLayer.move(group, ElementPlacement.INSIDE);
        
        // Set group invisible.
        group.visible = false;

    }

    csv.close();
    
    alert('Complete.');
    
} catch(e) {
    alert(e);
}
