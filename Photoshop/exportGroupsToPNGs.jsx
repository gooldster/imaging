/**
 * A Photoshop script for exporting Groups (aka LayerSets) to separate .png files.
 * 
 * For this specific usage you must:
 *  - Put all layers into a Group/LayerSet
 *  - Not have any similarly-named .png files in the same folder
 * 
 * @link https://bitbucket.org/gooldster/imaging/src/master/
 * @author Aaron Goold.
 * 
 */
if (documents.length == 0) {
    alert("No documents open.");
} else {
    try {
        var groups = activeDocument.layers;
        
        for(var i = 0; i < groups.length; i++) {
            groups[i].visible = false;
        }

        for(var i = 0; i < groups.length; i++) {
            
            groups[i].visible = true;

            var pngOptions = new PNGSaveOptions();
            pngOptions.compression = 9;
            
            var exportFile = new File(activeDocument.path + "/" + groups[i].name + ".png"); 
            activeDocument.saveAs(exportFile, pngOptions, true, Extension.LOWERCASE);

            groups[i].visible = false;
        }
        alert('Complete.');
    } catch (error) {
        alert("Error message: " + error);
    }
}
